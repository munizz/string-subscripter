export const toSubscript = (text: string) => {
  const SUBSCRIPTS =
    "        ₍₎ ₊ ₋  ₀₁₂₃₄₅₆₇₈₉   ₌                                   ₐ   ₑ  ₕᵢⱼₖₗₘₙₒₚ ᵣₛₜᵤᵥ ₓ       ";
  let result = text;

  Array.from(text).forEach(char => {
    const subscriptedChar =
      SUBSCRIPTS[text.charCodeAt(text.indexOf(char)) - 32] || " ";
    result =
      subscriptedChar === " " ? result : result.replace(char, subscriptedChar);
  });

  return result;
};
